# views.py
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from accounts.forms import LoginForm, SignUpForm
from django.contrib.auth.models import User


def user_login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect("list_projects")

    else:
        form = LoginForm()
    context = {
        "form": form,
    }

    return render(request, "accounts/login.html", context)


def user_logout(request):
    logout(request)
    return redirect("login")


def signup(request):
    if request.method == "GET":
        form = SignUpForm()
        return render(request, "accounts/login.html", {"form": form})

    elif request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password != password_confirmation:
                form.add_error(None, "The passwords do not match")
                return render(request, "accounts/signup.html", {"form": form})

            user = User.objects.create_user(username, password=password)
            user.save()

            login(request, user)

            return redirect("list_projects")
        else:
            return render(request, "accounts/signup.html", {"form": form})

    else:
        form = SignUpForm()
        return render(request, "accounts/signup.html", {"form": form})
